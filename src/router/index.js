import Vue from 'vue'
import Router from 'vue-router'
import posts from '../components/Posts'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'posts',
      component: posts
    }
  ]
})
